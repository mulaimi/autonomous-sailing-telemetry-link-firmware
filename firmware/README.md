# Sailing Boat Firmware


## Initialize West
in the current directory run

```
west init -l app
west update
```

## Building
in the current directory run

```
west build -b nucleof767zi app --pristine
west build -b nrf52833dk_nrf52833 app --pristine
```
