/*
 * Copyright (c) 2022, Silvano Cortesi
 * Copyright (c) 2022, ETH Zürich
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef BLE_H_
#define BLE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr/bluetooth/gatt.h>
#include <zephyr/sys/atomic.h>

int ble_init(struct k_msgq *ble_state_queue_param);
void ble_disconnect_all(void);

#ifdef __cplusplus
}
#endif

#endif
