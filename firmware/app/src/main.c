/*
 * Copyright (c) 2022 PBL
 */

#include <zephyr/logging/log.h>
#include <zephyr/kernel.h>
#include <zephyr/device.h>

#include "ble_services/health_thermometer_service.h"
#include "ble.h"

LOG_MODULE_REGISTER(MAIN_THREAD);

/* message queue to share ble connection state with the main thread */
K_MSGQ_DEFINE(ble_state_queue, sizeof(uint8_t), 1, sizeof(uint8_t));

void main(void)
{
	int ret = 0;
	uint8_t ble_state = 0;
	int16_t dummy[HEALTH_THERMOMETER_BUFFER_SIZE];
	dummy[0] = 0;
	
	LOG_INF("Hello World Log");
	ret = ble_init(&ble_state_queue);
	
	while (1) {
		LOG_INF("Loop count++");
		k_msgq_get(&ble_state_queue, &ble_state, K_NO_WAIT);
		k_sleep(K_SECONDS(1));
		ret = bt_gatt_body_temperature_notify(dummy);
		dummy[0]++;
	}
}
