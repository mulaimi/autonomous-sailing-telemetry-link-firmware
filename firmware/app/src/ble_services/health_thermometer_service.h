/*
 * Copyright (c) 2022, Silvano Cortesi
 * Copyright (c) 2022, ETH Zürich
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef BT_GATT_HEALTH_THERMOMETER_H_
#define BT_GATT_HEALTH_THERMOMETER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <zephyr/bluetooth/uuid.h>
#include <zephyr/types.h>

// Sensor values consists of measurements as big as two int. However, the sensor
// needs only 16bits, thus the size of the buffer is in int16_t. Additionally,
// one uint32_t is needed for message counting.
#define HEALTH_THERMOMETER_BUFFER_SIZE 10

// Health Thermometer Service UUID
#define HEALTH_THERMOMETER_SERVICE_UUID_VAL 0x1809
#define HEALTH_THERMOMETER_SERVICE_UUID                                        \
  BT_UUID_DECLARE_16(HEALTH_THERMOMETER_SERVICE_UUID_VAL)

// Temperature Characteristic UUID
#define TEMPERATURE_CHARACTERISTIC_UUID                                        \
  BT_UUID_DECLARE_128(                                                         \
      BT_UUID_128_ENCODE(0x18095c47, 0x81d2, 0x44e5, 0xa350, 0xaef131810001))

// This fct notifies the body temperature state to the connected BLE unit
// one additional uint32_t for counter state
int bt_gatt_body_temperature_notify(
    int16_t send_buffer[HEALTH_THERMOMETER_BUFFER_SIZE]);

#ifdef __cplusplus
}
#endif

#endif
