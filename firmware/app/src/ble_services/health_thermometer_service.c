/*
 * Copyright (c) 2022, Silvano Cortesi
 * Copyright (c) 2022, ETH Zürich
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <zephyr/bluetooth/gatt.h>
#include <errno.h>
#include <zephyr/logging/log.h>
#include <zephyr/kernel.h>

#include "health_thermometer_service.h"

LOG_MODULE_REGISTER(BLE_HEALTH_THERMOMETER_SERVICE);

static bool body_temperature_notification_enabled;

static void
body_temperature_characteristic_ccc_cfg(const struct bt_gatt_attr *attr,
                                        uint16_t value) {
  body_temperature_notification_enabled = (value == BT_GATT_CCC_NOTIFY);
  LOG_INF("Body Temperature notification %s",
          body_temperature_notification_enabled ? "enabled" : "disabled");
}

BT_GATT_SERVICE_DEFINE(
    health_thermometer_svc,
    BT_GATT_PRIMARY_SERVICE(HEALTH_THERMOMETER_SERVICE_UUID),
    BT_GATT_CHARACTERISTIC(TEMPERATURE_CHARACTERISTIC_UUID, BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_NONE, NULL, NULL, NULL),
    BT_GATT_CUD("Body Temperature Sensor - 30 samples, 1s", BT_GATT_PERM_READ_AUTHEN),
    BT_GATT_CCC(body_temperature_characteristic_ccc_cfg, BT_GATT_PERM_READ_AUTHEN | BT_GATT_PERM_WRITE_AUTHEN));

int bt_gatt_body_temperature_notify(int16_t send_buffer[HEALTH_THERMOMETER_BUFFER_SIZE]) {
  struct bt_gatt_attr *attr;
  attr = bt_gatt_find_by_uuid(health_thermometer_svc.attrs, 0, TEMPERATURE_CHARACTERISTIC_UUID);
  
  if (!body_temperature_notification_enabled) {
    return -EACCES;
  }
  return bt_gatt_notify(NULL, attr, send_buffer, HEALTH_THERMOMETER_BUFFER_SIZE * sizeof(int16_t));
}
