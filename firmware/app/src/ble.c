/*
 * Copyright (c) 2022, Silvano Cortesi
 * Copyright (c) 2022, ETH Zürich
 *
 * SPDX-License-Identifier: Apache-2.0
 */
#include <zephyr/logging/log.h>
#include <zephyr/settings/settings.h>

#include "ble.h"

LOG_MODULE_REGISTER(BLE_THREAD);

/* BLE setup */
#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

#define BT_LE_ADV_OPTIONS                                                      \
  BT_LE_ADV_PARAM(BT_LE_ADV_OPT_CONNECTABLE, BT_GAP_ADV_SLOW_INT_MIN,          \
                  BT_GAP_ADV_SLOW_INT_MAX, NULL)

static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

static struct k_msgq *ble_state_queue;

static void connected(struct bt_conn *conn, uint8_t err) {
  char addr[BT_ADDR_LE_STR_LEN];
  int ret = 0;

  bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));
  if (err < 0) {
    LOG_ERR("Connection failed (reason 0x%02x)", err);
    return;
  }

  LOG_INF("Connected to %s", addr);
  if (ret < 0) {
    LOG_ERR("Failed to set security (err %d). Disconnecting", ret);
    bt_conn_disconnect(conn, BT_HCI_ERR_AUTH_FAIL);
  }

  /* If no error, blink to show connected */
  if (ret >= 0) {
    uint8_t ble_connected = true;
    /* write value to queue */
    while (k_msgq_put(ble_state_queue, &ble_connected, K_NO_WAIT) != 0) {
      /* message queue is full: purge old data & try again */
      k_msgq_purge(ble_state_queue);
    }
  }
}

static void disconnected(struct bt_conn *conn, uint8_t reason) {
  char addr[BT_ADDR_LE_STR_LEN];

  bt_addr_le_to_str(bt_conn_get_dst(conn), addr, sizeof(addr));
  LOG_INF("Disconnected from %s (reason 0x%02x)", (addr), reason);
  uint8_t ble_connected = false;
  /* write value to queue */
  while (k_msgq_put(ble_state_queue, &ble_connected, K_NO_WAIT) != 0) {
    /* message queue is full: purge old data & try again */
    k_msgq_purge(ble_state_queue);
  }
}

static void le_param_updated(struct bt_conn *conn, uint16_t interval,
                             uint16_t latency, uint16_t timeout) {
  LOG_INF("Connection parameters updated. - interval: %d, latency: %d, "
          "timeout: %d",
          interval, latency, timeout);
}

BT_CONN_CB_DEFINE(conn_callbacks) = {
    .connected = connected,
    .disconnected = disconnected,
    .le_param_updated = le_param_updated
};

static void mtu_updated(struct bt_conn *conn, uint16_t tx, uint16_t rx) {
  LOG_INF("Updated MTU: TX: %d RX: %d bytes", tx, rx);
}

static struct bt_gatt_cb gatt_callbacks = {.att_mtu_updated = mtu_updated};

int ble_init(struct k_msgq *ble_state_queue_param) {
  int ret = 0;

  LOG_INF("Starting BLE (Init)");

  ble_state_queue = ble_state_queue_param;

  ret = bt_enable(NULL);
  if (ret < 0) {
    LOG_ERR("Init failed (err %d)", ret);
    return ret;
  }
  
  bt_gatt_cb_register(&gatt_callbacks);

  ret = bt_le_adv_start(BT_LE_ADV_OPTIONS, ad, ARRAY_SIZE(ad), NULL, 0);
  if (ret < 0) {
    LOG_ERR("Advertising failed to start (err %d)", ret);
    return ret;
  }

  LOG_INF("Advertising successfully started");

  return ret;
}

void ble_disconnect_all(void) {
  bt_conn_foreach(BT_CONN_TYPE_ALL,
                  (void (*)(struct bt_conn *, void *))bt_conn_disconnect,
                  (void *)BT_HCI_ERR_REMOTE_USER_TERM_CONN);
  k_sleep(K_MSEC(10));
}
