
from pymavlink.dialects.v20 import common as mavlink
import numpy as np
import serial
import time

indir = r"/home/mulaimi/Schreibtisch/Daten/221231_sailing_range_test/test06_01_2023_172353.log"

infile = open(indir)

class fifo(object):
    def __init__(self):
        self.buf = []
    def write(self, data):
        self.buf += data
        return len(data)
    def read(self):
        return self.buf.pop(0)

#create mavlink object
f = fifo()
mav = mavlink.MAVLink(f)
go = True

X = []
Y = []
R = []
XACC = []
YACC = []
XGYRO = []
YGYRO = []

while(go):
	
	text_line = infile.readline()
	if not text_line:
		go = False
	
	# ~ if(text_line == '\n'):
		# ~ continue
		
	if (";" in text_line):		
		if(":" in text_line):
			continue
		#print(text_line)
		res = text_line.split(";")
		msg_mavlink_hex = res[0].strip()
		msg_rssi = int(res[1].strip())

		#convert mavlink ascii-hex to binary and decode
		msg_mavlink = bytearray.fromhex(msg_mavlink_hex)
		m2 = mav.decode(msg_mavlink)
		# ~ if(m2.get_msgId() == 26): #IMU scaled
			# ~ #print("RSSI: "+str(msg_rssi)+"; "+str(m2))
			# ~ XACC.append(m2.xacc)
			# ~ YACC.append(m2.yacc)
			# ~ XGYRO.append(m2.xgyro)
			# ~ YGYRO.append(m2.xgyro)
			
		if(m2.get_msgId() == 33): #GPS
			#print("RSSI: "+str(msg_rssi)+"; "+str(m2))
			X.append(m2.lon)
			Y.append(m2.lat)
			R.append(msg_rssi)
			
x = np.array(X)
y = np.array(Y)
r = np.array(R)
x = np.reshape(x.astype(int), (len(x),1))
y = np.reshape(y.astype(int), (len(y),1))
r = np.reshape(r.astype(int), (len(r),1))
xyr = np.concatenate((x, y, r), axis=1)

# ~ xacc = np.array(XACC)
# ~ yacc = np.array(YACC)
# ~ xgyro = np.array(XGYRO)
# ~ ygyro = np.array(YGYRO)

# ~ xacc = np.reshape(xacc.astype(float), (len(xacc),1))
# ~ yacc = np.reshape(yacc.astype(float), (len(xacc),1))
# ~ xgyro = np.reshape(xgyro.astype(float), (len(xacc),1))
# ~ ygyro = np.reshape(ygyro.astype(float), (len(xacc),1))
# ~ imu = np.concatenate((xacc, yacc, xgyro, ygyro), axis=1)
#print(xyr)
#np.savetxt("extracted_1.txt", imu, fmt="%f")
np.savetxt("extracted_6.txt", xyr, fmt="%f")
print("Data extracting done!")
infile.close()
