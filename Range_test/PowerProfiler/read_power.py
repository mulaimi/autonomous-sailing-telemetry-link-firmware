import numpy as np
import matplotlib.pyplot as plt

data1 = np.loadtxt("0501_no_gps_with_sensors_BT_advertising.csv", delimiter=',', dtype=float)
data2 = np.loadtxt("0501_no_gps_with_sensors_BT_streaming.csv", delimiter=',', dtype=float)

np.set_printoptions(suppress=True)

curmean1 = np.mean(data1[:,1])/1000
curmean2 = np.mean(data2[:,1])/1000

# print values of each
print("Mean current over all time (without GPS and with sensor) while advertising: ", curmean1, "mA")
print("Average power consumption without V = 5V (without GPS and with sensor): ", curmean1 *5, "mW")
print("Mean current over all time (without GPS and sensor): ", curmean2, "mA")
print("Average power consumption with V = 5V (without GPS and with sensor) while streaming : ", curmean2 *5, "mW")

# print difference of no GPS, sensors on streaming and advertising
print("Difference of mean current between streaming and advertising over all time : ", np.abs(curmean1 - curmean2), "mA")
print("Difference of average power consumption between streaming and advertising with V = 5V: ", np.abs(curmean1 - curmean2)*5, "mW")


t1 = data1[:, 0]/1000
y1 = data1[:, 1]/1000

t2 = data2[:, 0]/1000
y2 = data2[:, 1]/1000

curmean1_arr = np.zeros(t1.shape)
curmean1_arr = curmean1_arr + curmean1
curmean2_arr = np.zeros(t2.shape)
curmean2_arr = curmean2_arr + curmean1

fig, ax = plt.subplots(2,1,figsize = (18,10))
ax[0].plot(t1[0:20000], y1[0:20000])
ax[0].plot(t1[0:20000], curmean1_arr[0:20000], color = 'r', linewidth = '2', label = 'average of %f mA' % curmean1)
ax[0].set(xlabel="time [s]", ylabel="current [mA]")
ax[0].set_title("Current measured over time without GPS and with sensors while advertising")
ax[0].legend()

ax[1].plot(t2[0:20000], y2[0:20000])
ax[1].plot(t2[0:20000], curmean2_arr[0:20000], color = 'r', linewidth = '2', label = 'average of %f mA' % curmean2)
ax[1].set(xlabel="time [s]", ylabel="current [mA]")
ax[1].set_title("Current measured over time without GPS and with sensors while streaming data")
ax[1].legend()

plt.savefig('plot_no_gps_with_sensor.png', bbox_inches='tight')


plt.show()



