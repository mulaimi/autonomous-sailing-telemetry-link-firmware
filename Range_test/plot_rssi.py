
"""
	Reads GPS and RSSI values extracted by read_data.py
	and calculates distance to reference point then plots trajectory
	on according map and RSSI value as function of distance
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import inflect

def test_func(x, a, b, c):

	return 20*a*np.log(b/x) + c


def calc_dist(ref, coord):

	"""
	Calculates distance to reference

	coord, ref: [lon,lat] coordinates
	returns calculated distance in meter
	"""

	length = 111300
	angle = (ref[1] + coord[1])/(2*180*10000000) * np.pi
	dx = length*np.cos(angle)*(np.abs(ref[0] - coord[0])/10000000)
	dy = length*(np.abs(ref[1] - coord[1])/10000000)

	return np.sqrt(dx * dx + dy * dy)


def plot_rssi(distance, rssi, walk):

	"""
	Plots RSSI as function of distance, depending on which walk

	distance:	1-D array with distances
	rssi:		1-D array with RSSI values
	walk:		walk number indicator
	"""
	start_indizes = [149, 59, 32, 40, 0, 15, 0]
	#start_fit_indizes = [160, 68, 80, 50, 18, 22, 47] # modified start idizes such that staying near reference point does not distort fitting
	max_index = np.where(distance == max(distance))[0][0]
	#print(start_indizes[walk-1], max_index)
	param, param_cov = curve_fit(test_func, distance[start_indizes[walk-1]:max_index], rssi[start_indizes[walk-1]:max_index])
	#param, param_cov = curve_fit(test_func, distance[start_fit_indizes[walk-1]:max_index], rssi[start_fit_indizes[walk-1]:max_index])


	if(walk == 1):
		plt.title('PHY 2M range vs. RSSI')

	else:
		plt.title('Coded PHY range vs. RSSI with 8 dBm TX-power')

	plt.scatter(distances[start_indizes[walk-1]:max_index], rssi[start_indizes[walk-1]:max_index])
	plt.plot(distance[start_indizes[walk-1]+10:max_index], test_func(distance[start_indizes[walk-1]+10:max_index], param[0], param[1], param[2]), color = 'r', linewidth = '2' , label='fit: y(x) ~ $20*log(x/x_0)$')
	plt.xlabel('distance[m]')
	plt.ylabel('RSSI[dBm]')
	plt.legend()
	plt.savefig('%s walk_rssi_plot.png' % p.number_to_words(p.ordinal(walks_counter - 1)), bbox_inches='tight')
	plt.show()

	return


def plot_trajectory(xy, walk):

	"""
	Plots trajectory of walk in according map
	xy:		2-D array with coodinates [lat,lon]
	walk:	walk number indicator
	"""

	blatt_map = plt.imread('blatterwiese.png')
	jetty_map = plt.imread('jetty.png')

	if(walk == 7):
		fig, ax = plt.subplots(figsize = (15,7))
		ax.set_title('The last walk with Coded PHY and 8 dBm TX-power')
		ax.scatter(xy[:,0], xy[:,1], zorder=1, alpha= 0.2, c='b', s=10)
		ax.set_xlim(8.55294, 8.55880)
		ax.set_ylim(47.35120, 47.35335)
		ax.imshow(jetty_map, zorder=0, extent = ((8.55294, 8.55880,47.35120, 47.35335)), aspect= 'equal')

	else:
		fig, ax = plt.subplots(figsize = (8, 7))

		if(walk == 1):
			ax.set_title('The first walk with PHY 2M')
			ax.scatter(xy[142:264,0], xy[142:264,1], zorder=1, alpha= 0.2, c='b', s=10)
		else:
			ax.set_title('The %s walk with Coded PHY and 8 dBm TX-power' % p.number_to_words(p.ordinal(walks_counter)))
			ax.scatter(xy[:,0], xy[:,1], zorder=1, alpha= 0.2, c='b', s=10)

		ax.set_xlim(8.54934, 8.55288)
		ax.set_ylim(47.35368, 47.35584)
		ax.imshow(blatt_map, zorder=0, extent = ((8.54934, 8.55288,47.35368, 47.35584)), aspect= 'equal')

	ax.set(xlabel = 'longitude[°]', ylabel = 'latitude[°]')
	plt.savefig('%s walk_trajectory_plot.png' % p.number_to_words(p.ordinal(walks_counter - 1)), bbox_inches='tight')
	plt.show()

	return

files = ["lonlat_rssi_first_walk_2M_clean.txt", "extracted_1_clean.txt", "extracted_2_clean.txt", "extracted_3_clean.txt",
		 "extracted_4_clean.txt", "extracted_5_clean.txt", "extracted_6_clean.txt"]
p = inflect.engine()
walks_counter = 1

for filename in files:

	xyr = np.loadtxt(filename).astype(np.int64)
	reference_bench_1 = np.array((85511800, 473540500))
	reference_bench_2 = np.array((85520745, 473539641))
	reference_jetty = np.array((85532091, 473522483))
	rssi = xyr[:,2]
	rssi = np.array(rssi.astype(int))
	distances = np.zeros(rssi.shape)

	if(walks_counter == 1):
		for i in range(0,len(rssi)):
			distances[i] = calc_dist(reference_bench_1, xyr[i,0:2])

	if(walks_counter==7):
		for i in range(0,len(rssi)):
			distances[i] = calc_dist(reference_jetty, xyr[i,0:2])

	else:
		for i in range(0,len(rssi)):
			distances[i] = calc_dist(reference_bench_2, xyr[i,0:2])

	print("Max distance on %s walk is: " % p.number_to_words(p.ordinal(walks_counter)) , max(distances), ", lowest appeared RSSI is: ", min(rssi))
	np.set_printoptions(suppress=True)

	plot_rssi(distances, rssi, walks_counter)

	xy = xyr[:, 0:2]/10000000
	plot_trajectory(xy, walks_counter)

	if(walks_counter != 1):
		np.savetxt("distances_%s_walk_realCoded.txt" % p.number_to_words(p.ordinal(walks_counter - 1)), distances, fmt="%f")

	walks_counter = walks_counter + 1

print("done")
